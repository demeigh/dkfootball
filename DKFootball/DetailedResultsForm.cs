﻿using System.Windows.Forms;

namespace DKFootball
{
    public partial class DetailedResultsForm : Form
    {
        public DetailedResultsForm()
        {
            InitializeComponent();
        }

        public void Teams(object teams)
        {
            BsDetailedDKTeams.DataSource = teams;
        }

        private void DetailedResultsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MainForm.RegularNameMode = true;
        }
    }
}

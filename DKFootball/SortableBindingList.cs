﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Linq;
using System.Linq.Expressions;

namespace DKFootball
{
    /// <summary>
    /// Provides a generic collection that supports data binding and additionally supports sorting.
    /// See http://msdn.microsoft.com/en-us/library/ms993236.aspx
    /// If the elements are IComparable it uses that; otherwise compares the ToString()
    /// </summary>
    /// <typeparam name="T">The type of elements in the list.</typeparam>
    public class SortableBindingList<T> : BindingList<T>, IBindingListView
    {
        private bool _isSorted;
        private ListSortDirection _sortDirection = ListSortDirection.Ascending;
        private PropertyDescriptor _sortProperty;
        
        protected override bool SupportsSortingCore => true;
        protected override bool IsSortedCore => _isSorted;
        protected override ListSortDirection SortDirectionCore => _sortDirection;
        protected override PropertyDescriptor SortPropertyCore => _sortProperty;
        
        public SortableBindingList()
        {
        }

        public SortableBindingList(IList<T> list) : base(list)
        {
        }
        
        protected override void RemoveSortCore()
        {
            _sortDirection = ListSortDirection.Ascending;
            _sortProperty = null;
            _isSorted = false; 
        }
        
        protected override void ApplySortCore(PropertyDescriptor prop, ListSortDirection direction)
        {
            _sortProperty = prop;
            _sortDirection = direction;

            List<T> list = Items as List<T>;
            if (list == null) return;

            list.Sort(Compare);

            _isSorted = true;
            //fire an event that the list has been changed.
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }
        
        private int Compare(T lhs, T rhs)
        {
            var result = OnComparison(lhs, rhs);
            //invert if descending
            if (_sortDirection == ListSortDirection.Descending)
                result = -result;
            return result;
        }

        private int OnComparison(T lhs, T rhs)
        {
            object lhsValue = lhs == null ? null : _sortProperty.GetValue(lhs);
            object rhsValue = rhs == null ? null : _sortProperty.GetValue(rhs);
            if (lhsValue == null)
            {
                return (rhsValue == null) ? 0 : -1; //nulls are equal
            }
            if (rhsValue == null)
            {
                return 1; //first has value, second doesn't
            }
            if (lhsValue is IComparable)
            {
                return ((IComparable)lhsValue).CompareTo(rhsValue);
            }
            if (lhsValue.Equals(rhsValue))
            {
                return 0; //both are the same
            }
            //not comparable, compare ToString
            return lhsValue.ToString().CompareTo(rhsValue.ToString());
        }

        
        //IBindingListView unimplemented implementation
        private string _filterValue = null;
        List<T> _unfilteredListValue = new List<T>();
        public bool SupportsFiltering => true;
        public bool SupportsAdvancedSorting => false;
        public ListSortDescriptionCollection SortDescriptions => null;

        

        public string Filter
        {
            get => _filterValue;

            set
            {
                if (_filterValue != value)
                {
                    RaiseListChangedEvents = false;

                    //resets list
                    if (value == null || value == "")
                    {
                        this.ClearItems();
                        foreach (T t in _unfilteredListValue)
                            this.Items.Add(t);
                        _filterValue = value;
                    }

                    else
                    {
                        // If the filter is not set.
                        _unfilteredListValue.Clear();
                        _unfilteredListValue.AddRange(this.Items);
                        _filterValue = value;
                        this.ClearItems();
                        
                        List<T> filterparts = GetFilterParts();
                        foreach (T filterResults in filterparts)
                        {
                            this.Add(filterResults);
                        }
                    }

                    RaiseListChangedEvents = true;

                    OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                }
            }
        }
        
        public List<T> GetFilterParts()
        {
            List<T> results = new List<T>();
            
            string[] linesOfFilters = _filterValue.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            var item = Expression.Parameter(typeof(T), "type");
            var finalEqual = Expression.Equal(Expression.Constant(1), Expression.Constant(1));

            foreach (string filt in linesOfFilters)
            {

                if (filt.Contains("%%"))
                {

                    string[] filtPart = filt.Split(new char[] { '%' }, StringSplitOptions.RemoveEmptyEntries);
                    string filterPropName = filtPart[0].Trim();
                    string filterCompareString = filtPart[1].Replace("'", "").Trim().ToLower();
                    Console.WriteLine(filterCompareString);
                    PropertyDescriptor propDesc = TypeDescriptor.GetProperties(typeof(T))[filterPropName];
                    TypeConverter converter = TypeDescriptor.GetConverter(propDesc.PropertyType);
                    
                    Object filterCompareVal = converter.ConvertFromString(filterCompareString.ToLower());
                    var prop3 = Expression.Property(item, filterPropName);
                    var soap3 = Expression.Constant(filterCompareVal);
                    
                    var toLower = Expression.Call(prop3, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                    var condition = Expression.Call(toLower, typeof(string).GetMethod("Contains"), soap3);
                    
                    finalEqual = Expression.AndAlso(finalEqual, condition);
                }
                else
                {

                    string[] filtPart = filt.Split(new char[] { '=' });
                    string filterPropName = filtPart[0].Trim();
                    string filterCompareString = filtPart[1].Replace("'", "").Trim();
                    PropertyDescriptor propDesc = TypeDescriptor.GetProperties(typeof(T))[filterPropName];
                    TypeConverter converter = TypeDescriptor.GetConverter(propDesc.PropertyType);


                    if (filterCompareString.Contains("||"))
                    {
                        string[] orCompareSplits = filterCompareString.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        var tempEqual = Expression.Equal(Expression.Constant(0), Expression.Constant(1));
                        foreach (string compareString in orCompareSplits)
                        {
                            Object filterCompareVal = converter.ConvertFromString(compareString);
                            var prop3 = Expression.Property(item, filterPropName);
                            var soap3 = Expression.Constant(filterCompareVal);
                            tempEqual = Expression.OrElse(tempEqual, Expression.Equal(prop3, soap3));
                        }
                        finalEqual = Expression.AndAlso(finalEqual, tempEqual);
                        Console.WriteLine(finalEqual.ToString());
                    }
                    else
                    {
                        Object filterCompareVal = converter.ConvertFromString(filterCompareString);
                        var prop3 = Expression.Property(item, filterPropName);
                        var soap3 = Expression.Constant(filterCompareVal);
                        finalEqual = Expression.AndAlso(finalEqual, Expression.Equal(prop3, soap3));
                    }
                }
                Console.WriteLine(finalEqual.ToString());
               // Console.WriteLine(filt+ " " + linesOfFilters.Length.ToString());

            }

            var lambda = Expression.Lambda<Func<T, bool>>(finalEqual, item);
            var resul = _unfilteredListValue.AsQueryable<T>().Where(lambda);
            results = resul.ToList();
            return results;
        }
        
        

        protected int FindCore(int startIndex, PropertyDescriptor prop, object key, string determiner)
        {
            // Get the property info for the specified property.
            PropertyInfo propInfo = typeof(T).GetProperty(prop.Name);
            T item;
            if (key != null)
            {
                // Loop through the items to see if the key
                // value matches the property value.
                for (int i = startIndex; i < Count; ++i)
                {
                    item = (T)Items[i];
                    if (determiner == "=" && propInfo.GetValue(item, null).Equals(key))
                    {
                        Console.WriteLine("somethign fishy");
                        return i;
                    }
                    else if (determiner == "%" && propInfo.GetValue(item, null).ToString().IndexOf((String)key, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return i;
                    }
                    else
                    {
                    }
                }
            }
            return -1;
        }
        
        public void RemoveFilter()
        {
            if (Filter != null) Filter = null;
        }
        
        
        public void ApplySort(ListSortDescriptionCollection sorts)
        {
            throw new NotImplementedException();
        }
    }
}

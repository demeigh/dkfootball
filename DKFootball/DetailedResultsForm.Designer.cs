﻿namespace DKFootball
{
    partial class DetailedResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DgvDetailedTeams = new System.Windows.Forms.DataGridView();
            this.qBDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rB1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rB2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wR1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wR2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wR3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLEXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teamPointsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.placedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.balanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BsDetailedDKTeams = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DgvDetailedTeams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsDetailedDKTeams)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvDetailedTeams
            // 
            this.DgvDetailedTeams.AllowUserToAddRows = false;
            this.DgvDetailedTeams.AllowUserToDeleteRows = false;
            this.DgvDetailedTeams.AllowUserToOrderColumns = true;
            this.DgvDetailedTeams.AutoGenerateColumns = false;
            this.DgvDetailedTeams.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvDetailedTeams.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.qBDataGridViewTextBoxColumn,
            this.rB1DataGridViewTextBoxColumn,
            this.rB2DataGridViewTextBoxColumn,
            this.wR1DataGridViewTextBoxColumn,
            this.wR2DataGridViewTextBoxColumn,
            this.wR3DataGridViewTextBoxColumn,
            this.tEDataGridViewTextBoxColumn,
            this.fLEXDataGridViewTextBoxColumn,
            this.DST,
            this.teamPointsDataGridViewTextBoxColumn,
            this.placedDataGridViewTextBoxColumn,
            this.balanceDataGridViewTextBoxColumn});
            this.DgvDetailedTeams.DataSource = this.BsDetailedDKTeams;
            this.DgvDetailedTeams.Location = new System.Drawing.Point(12, 12);
            this.DgvDetailedTeams.Name = "DgvDetailedTeams";
            this.DgvDetailedTeams.ReadOnly = true;
            this.DgvDetailedTeams.Size = new System.Drawing.Size(1617, 636);
            this.DgvDetailedTeams.TabIndex = 0;
            // 
            // qBDataGridViewTextBoxColumn
            // 
            this.qBDataGridViewTextBoxColumn.DataPropertyName = "QB";
            this.qBDataGridViewTextBoxColumn.HeaderText = "QB";
            this.qBDataGridViewTextBoxColumn.Name = "qBDataGridViewTextBoxColumn";
            this.qBDataGridViewTextBoxColumn.ReadOnly = true;
            this.qBDataGridViewTextBoxColumn.Width = 150;
            // 
            // rB1DataGridViewTextBoxColumn
            // 
            this.rB1DataGridViewTextBoxColumn.DataPropertyName = "RB1";
            this.rB1DataGridViewTextBoxColumn.HeaderText = "RB1";
            this.rB1DataGridViewTextBoxColumn.Name = "rB1DataGridViewTextBoxColumn";
            this.rB1DataGridViewTextBoxColumn.ReadOnly = true;
            this.rB1DataGridViewTextBoxColumn.Width = 150;
            // 
            // rB2DataGridViewTextBoxColumn
            // 
            this.rB2DataGridViewTextBoxColumn.DataPropertyName = "RB2";
            this.rB2DataGridViewTextBoxColumn.HeaderText = "RB2";
            this.rB2DataGridViewTextBoxColumn.Name = "rB2DataGridViewTextBoxColumn";
            this.rB2DataGridViewTextBoxColumn.ReadOnly = true;
            this.rB2DataGridViewTextBoxColumn.Width = 150;
            // 
            // wR1DataGridViewTextBoxColumn
            // 
            this.wR1DataGridViewTextBoxColumn.DataPropertyName = "WR1";
            this.wR1DataGridViewTextBoxColumn.HeaderText = "WR1";
            this.wR1DataGridViewTextBoxColumn.Name = "wR1DataGridViewTextBoxColumn";
            this.wR1DataGridViewTextBoxColumn.ReadOnly = true;
            this.wR1DataGridViewTextBoxColumn.Width = 150;
            // 
            // wR2DataGridViewTextBoxColumn
            // 
            this.wR2DataGridViewTextBoxColumn.DataPropertyName = "WR2";
            this.wR2DataGridViewTextBoxColumn.HeaderText = "WR2";
            this.wR2DataGridViewTextBoxColumn.Name = "wR2DataGridViewTextBoxColumn";
            this.wR2DataGridViewTextBoxColumn.ReadOnly = true;
            this.wR2DataGridViewTextBoxColumn.Width = 150;
            // 
            // wR3DataGridViewTextBoxColumn
            // 
            this.wR3DataGridViewTextBoxColumn.DataPropertyName = "WR3";
            this.wR3DataGridViewTextBoxColumn.HeaderText = "WR3";
            this.wR3DataGridViewTextBoxColumn.Name = "wR3DataGridViewTextBoxColumn";
            this.wR3DataGridViewTextBoxColumn.ReadOnly = true;
            this.wR3DataGridViewTextBoxColumn.Width = 150;
            // 
            // tEDataGridViewTextBoxColumn
            // 
            this.tEDataGridViewTextBoxColumn.DataPropertyName = "TE";
            this.tEDataGridViewTextBoxColumn.HeaderText = "TE";
            this.tEDataGridViewTextBoxColumn.Name = "tEDataGridViewTextBoxColumn";
            this.tEDataGridViewTextBoxColumn.ReadOnly = true;
            this.tEDataGridViewTextBoxColumn.Width = 150;
            // 
            // fLEXDataGridViewTextBoxColumn
            // 
            this.fLEXDataGridViewTextBoxColumn.DataPropertyName = "FLEX";
            this.fLEXDataGridViewTextBoxColumn.HeaderText = "FLEX";
            this.fLEXDataGridViewTextBoxColumn.Name = "fLEXDataGridViewTextBoxColumn";
            this.fLEXDataGridViewTextBoxColumn.ReadOnly = true;
            this.fLEXDataGridViewTextBoxColumn.Width = 150;
            // 
            // DST
            // 
            this.DST.DataPropertyName = "DST";
            this.DST.HeaderText = "DST";
            this.DST.Name = "DST";
            this.DST.ReadOnly = true;
            this.DST.Width = 150;
            // 
            // teamPointsDataGridViewTextBoxColumn
            // 
            this.teamPointsDataGridViewTextBoxColumn.DataPropertyName = "TeamPoints";
            this.teamPointsDataGridViewTextBoxColumn.HeaderText = "TeamPoints";
            this.teamPointsDataGridViewTextBoxColumn.Name = "teamPointsDataGridViewTextBoxColumn";
            this.teamPointsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // placedDataGridViewTextBoxColumn
            // 
            this.placedDataGridViewTextBoxColumn.DataPropertyName = "Placed";
            this.placedDataGridViewTextBoxColumn.HeaderText = "Placed";
            this.placedDataGridViewTextBoxColumn.Name = "placedDataGridViewTextBoxColumn";
            this.placedDataGridViewTextBoxColumn.ReadOnly = true;
            this.placedDataGridViewTextBoxColumn.Width = 50;
            // 
            // balanceDataGridViewTextBoxColumn
            // 
            this.balanceDataGridViewTextBoxColumn.DataPropertyName = "Balance";
            this.balanceDataGridViewTextBoxColumn.HeaderText = "Balance";
            this.balanceDataGridViewTextBoxColumn.Name = "balanceDataGridViewTextBoxColumn";
            this.balanceDataGridViewTextBoxColumn.ReadOnly = true;
            this.balanceDataGridViewTextBoxColumn.Width = 50;
            // 
            // BsDetailedDKTeams
            // 
            this.BsDetailedDKTeams.DataSource = typeof(DKFootball.DKTeam);
            // 
            // DetailedResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1641, 660);
            this.Controls.Add(this.DgvDetailedTeams);
            this.Name = "DetailedResultsForm";
            this.Text = "OptionsForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DetailedResultsForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.DgvDetailedTeams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsDetailedDKTeams)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DgvDetailedTeams;
        private System.Windows.Forms.BindingSource BsDetailedDKTeams;
        private System.Windows.Forms.DataGridViewTextBoxColumn qBDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rB1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rB2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wR1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wR2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wR3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLEXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DST;
        private System.Windows.Forms.DataGridViewTextBoxColumn teamPointsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn placedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn balanceDataGridViewTextBoxColumn;
    }
}
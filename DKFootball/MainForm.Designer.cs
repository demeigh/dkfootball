﻿namespace DKFootball
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BtnOpen = new System.Windows.Forms.Button();
            this.DgvAllPlayers = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalTeamsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moneySpentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PointsScored = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PercentOwned = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Owned = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Starred = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ShowTeams = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BsAllDKPlayers = new System.Windows.Forms.BindingSource(this.components);
            this.DgvTeams = new System.Windows.Forms.DataGridView();
            this.qBDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rB1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rB2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wR1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wR2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wR3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLEXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dSTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.balanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contestCostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BsAllDKTeams = new System.Windows.Forms.BindingSource(this.components);
            this.ClbPositions = new System.Windows.Forms.CheckedListBox();
            this.BtnSportMode = new System.Windows.Forms.Button();
            this.BtnResults = new System.Windows.Forms.Button();
            this.BtnResultsDetails = new System.Windows.Forms.Button();
            this.TbSearchBox = new System.Windows.Forms.TextBox();
            this.CbOwned = new System.Windows.Forms.CheckBox();
            this.BtnSwapPlayers = new System.Windows.Forms.Button();
            this.BtnExportCSV = new System.Windows.Forms.Button();
            this.BtnSwitchTeams = new System.Windows.Forms.Button();
            this.CbStarred = new System.Windows.Forms.CheckBox();
            this.BtnContestMode = new System.Windows.Forms.Button();
            this.CbShowCheckedTeams = new System.Windows.Forms.CheckBox();
            this.BtnSaveState = new System.Windows.Forms.Button();
            this.BtnLoadState = new System.Windows.Forms.Button();
            this.BtnImportLineupsCSV = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAllPlayers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsAllDKPlayers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTeams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsAllDKTeams)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnOpen
            // 
            this.BtnOpen.Location = new System.Drawing.Point(887, 12);
            this.BtnOpen.Name = "BtnOpen";
            this.BtnOpen.Size = new System.Drawing.Size(75, 23);
            this.BtnOpen.TabIndex = 0;
            this.BtnOpen.Text = "OPEN";
            this.BtnOpen.UseVisualStyleBackColor = true;
            this.BtnOpen.Click += new System.EventHandler(this.BtnOpen_Click);
            // 
            // DgvAllPlayers
            // 
            this.DgvAllPlayers.AllowUserToAddRows = false;
            this.DgvAllPlayers.AllowUserToDeleteRows = false;
            this.DgvAllPlayers.AllowUserToOrderColumns = true;
            this.DgvAllPlayers.AutoGenerateColumns = false;
            this.DgvAllPlayers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvAllPlayers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.positionDataGridViewTextBoxColumn,
            this.salaryDataGridViewTextBoxColumn,
            this.teamDataGridViewTextBoxColumn,
            this.totalTeamsDataGridViewTextBoxColumn,
            this.moneySpentDataGridViewTextBoxColumn,
            this.PointsScored,
            this.PercentOwned,
            this.Owned,
            this.Starred,
            this.ShowTeams});
            this.DgvAllPlayers.DataSource = this.BsAllDKPlayers;
            this.DgvAllPlayers.Location = new System.Drawing.Point(13, 12);
            this.DgvAllPlayers.MultiSelect = false;
            this.DgvAllPlayers.Name = "DgvAllPlayers";
            this.DgvAllPlayers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvAllPlayers.Size = new System.Drawing.Size(868, 250);
            this.DgvAllPlayers.TabIndex = 1;
            this.DgvAllPlayers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvAllPlayers_CellDoubleClick);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 150;
            // 
            // positionDataGridViewTextBoxColumn
            // 
            this.positionDataGridViewTextBoxColumn.DataPropertyName = "Position";
            this.positionDataGridViewTextBoxColumn.HeaderText = "POS";
            this.positionDataGridViewTextBoxColumn.Name = "positionDataGridViewTextBoxColumn";
            this.positionDataGridViewTextBoxColumn.ReadOnly = true;
            this.positionDataGridViewTextBoxColumn.Width = 50;
            // 
            // salaryDataGridViewTextBoxColumn
            // 
            this.salaryDataGridViewTextBoxColumn.DataPropertyName = "Salary";
            this.salaryDataGridViewTextBoxColumn.HeaderText = "SAL";
            this.salaryDataGridViewTextBoxColumn.Name = "salaryDataGridViewTextBoxColumn";
            this.salaryDataGridViewTextBoxColumn.ReadOnly = true;
            this.salaryDataGridViewTextBoxColumn.Width = 50;
            // 
            // teamDataGridViewTextBoxColumn
            // 
            this.teamDataGridViewTextBoxColumn.DataPropertyName = "Team";
            this.teamDataGridViewTextBoxColumn.HeaderText = "Team";
            this.teamDataGridViewTextBoxColumn.Name = "teamDataGridViewTextBoxColumn";
            this.teamDataGridViewTextBoxColumn.ReadOnly = true;
            this.teamDataGridViewTextBoxColumn.Width = 50;
            // 
            // totalTeamsDataGridViewTextBoxColumn
            // 
            this.totalTeamsDataGridViewTextBoxColumn.DataPropertyName = "TotalTeams";
            this.totalTeamsDataGridViewTextBoxColumn.HeaderText = "###";
            this.totalTeamsDataGridViewTextBoxColumn.Name = "totalTeamsDataGridViewTextBoxColumn";
            this.totalTeamsDataGridViewTextBoxColumn.ReadOnly = true;
            this.totalTeamsDataGridViewTextBoxColumn.Width = 50;
            // 
            // moneySpentDataGridViewTextBoxColumn
            // 
            this.moneySpentDataGridViewTextBoxColumn.DataPropertyName = "MoneySpent";
            this.moneySpentDataGridViewTextBoxColumn.HeaderText = "$$$";
            this.moneySpentDataGridViewTextBoxColumn.Name = "moneySpentDataGridViewTextBoxColumn";
            this.moneySpentDataGridViewTextBoxColumn.ReadOnly = true;
            this.moneySpentDataGridViewTextBoxColumn.Width = 50;
            // 
            // PointsScored
            // 
            this.PointsScored.DataPropertyName = "PointsScored";
            this.PointsScored.HeaderText = "FPTS";
            this.PointsScored.Name = "PointsScored";
            this.PointsScored.ReadOnly = true;
            this.PointsScored.Width = 50;
            // 
            // PercentOwned
            // 
            this.PercentOwned.DataPropertyName = "PercentOwned";
            this.PercentOwned.HeaderText = "%%%";
            this.PercentOwned.Name = "PercentOwned";
            this.PercentOwned.ReadOnly = true;
            this.PercentOwned.Width = 50;
            // 
            // Owned
            // 
            this.Owned.DataPropertyName = "Owned";
            this.Owned.HeaderText = "OWN";
            this.Owned.Name = "Owned";
            this.Owned.ReadOnly = true;
            this.Owned.Width = 50;
            // 
            // Starred
            // 
            this.Starred.DataPropertyName = "Starred";
            this.Starred.HeaderText = "***";
            this.Starred.Name = "Starred";
            this.Starred.Width = 50;
            // 
            // ShowTeams
            // 
            this.ShowTeams.DataPropertyName = "ShowTeams";
            this.ShowTeams.HeaderText = "TEAMS";
            this.ShowTeams.Name = "ShowTeams";
            this.ShowTeams.Width = 50;
            // 
            // BsAllDKPlayers
            // 
            this.BsAllDKPlayers.DataSource = typeof(DKFootball.DKPlayer);
            this.BsAllDKPlayers.Sort = "";
            // 
            // DgvTeams
            // 
            this.DgvTeams.AllowUserToAddRows = false;
            this.DgvTeams.AllowUserToDeleteRows = false;
            this.DgvTeams.AllowUserToOrderColumns = true;
            this.DgvTeams.AutoGenerateColumns = false;
            this.DgvTeams.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTeams.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.qBDataGridViewTextBoxColumn,
            this.rB1DataGridViewTextBoxColumn,
            this.rB2DataGridViewTextBoxColumn,
            this.wR1DataGridViewTextBoxColumn,
            this.wR2DataGridViewTextBoxColumn,
            this.wR3DataGridViewTextBoxColumn,
            this.tEDataGridViewTextBoxColumn,
            this.fLEXDataGridViewTextBoxColumn,
            this.dSTDataGridViewTextBoxColumn,
            this.balanceDataGridViewTextBoxColumn,
            this.contestCostDataGridViewTextBoxColumn});
            this.DgvTeams.DataSource = this.BsAllDKTeams;
            this.DgvTeams.Location = new System.Drawing.Point(13, 268);
            this.DgvTeams.Name = "DgvTeams";
            this.DgvTeams.ReadOnly = true;
            this.DgvTeams.Size = new System.Drawing.Size(1273, 324);
            this.DgvTeams.TabIndex = 2;
            this.DgvTeams.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvTeams_CellDoubleClick);
            // 
            // qBDataGridViewTextBoxColumn
            // 
            this.qBDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.qBDataGridViewTextBoxColumn.DataPropertyName = "QB";
            this.qBDataGridViewTextBoxColumn.HeaderText = "QB";
            this.qBDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.qBDataGridViewTextBoxColumn.Name = "qBDataGridViewTextBoxColumn";
            this.qBDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rB1DataGridViewTextBoxColumn
            // 
            this.rB1DataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.rB1DataGridViewTextBoxColumn.DataPropertyName = "RB1";
            this.rB1DataGridViewTextBoxColumn.HeaderText = "RB1";
            this.rB1DataGridViewTextBoxColumn.MinimumWidth = 100;
            this.rB1DataGridViewTextBoxColumn.Name = "rB1DataGridViewTextBoxColumn";
            this.rB1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rB2DataGridViewTextBoxColumn
            // 
            this.rB2DataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.rB2DataGridViewTextBoxColumn.DataPropertyName = "RB2";
            this.rB2DataGridViewTextBoxColumn.HeaderText = "RB2";
            this.rB2DataGridViewTextBoxColumn.MinimumWidth = 100;
            this.rB2DataGridViewTextBoxColumn.Name = "rB2DataGridViewTextBoxColumn";
            this.rB2DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // wR1DataGridViewTextBoxColumn
            // 
            this.wR1DataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.wR1DataGridViewTextBoxColumn.DataPropertyName = "WR1";
            this.wR1DataGridViewTextBoxColumn.HeaderText = "WR1";
            this.wR1DataGridViewTextBoxColumn.MinimumWidth = 100;
            this.wR1DataGridViewTextBoxColumn.Name = "wR1DataGridViewTextBoxColumn";
            this.wR1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // wR2DataGridViewTextBoxColumn
            // 
            this.wR2DataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.wR2DataGridViewTextBoxColumn.DataPropertyName = "WR2";
            this.wR2DataGridViewTextBoxColumn.HeaderText = "WR2";
            this.wR2DataGridViewTextBoxColumn.MinimumWidth = 100;
            this.wR2DataGridViewTextBoxColumn.Name = "wR2DataGridViewTextBoxColumn";
            this.wR2DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // wR3DataGridViewTextBoxColumn
            // 
            this.wR3DataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.wR3DataGridViewTextBoxColumn.DataPropertyName = "WR3";
            this.wR3DataGridViewTextBoxColumn.HeaderText = "WR3";
            this.wR3DataGridViewTextBoxColumn.MinimumWidth = 100;
            this.wR3DataGridViewTextBoxColumn.Name = "wR3DataGridViewTextBoxColumn";
            this.wR3DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tEDataGridViewTextBoxColumn
            // 
            this.tEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.tEDataGridViewTextBoxColumn.DataPropertyName = "TE";
            this.tEDataGridViewTextBoxColumn.HeaderText = "TE";
            this.tEDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.tEDataGridViewTextBoxColumn.Name = "tEDataGridViewTextBoxColumn";
            this.tEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fLEXDataGridViewTextBoxColumn
            // 
            this.fLEXDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.fLEXDataGridViewTextBoxColumn.DataPropertyName = "FLEX";
            this.fLEXDataGridViewTextBoxColumn.HeaderText = "FLEX";
            this.fLEXDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.fLEXDataGridViewTextBoxColumn.Name = "fLEXDataGridViewTextBoxColumn";
            this.fLEXDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dSTDataGridViewTextBoxColumn
            // 
            this.dSTDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.dSTDataGridViewTextBoxColumn.DataPropertyName = "DST";
            this.dSTDataGridViewTextBoxColumn.HeaderText = "DST";
            this.dSTDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.dSTDataGridViewTextBoxColumn.Name = "dSTDataGridViewTextBoxColumn";
            this.dSTDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // balanceDataGridViewTextBoxColumn
            // 
            this.balanceDataGridViewTextBoxColumn.DataPropertyName = "Balance";
            this.balanceDataGridViewTextBoxColumn.HeaderText = "Balance";
            this.balanceDataGridViewTextBoxColumn.Name = "balanceDataGridViewTextBoxColumn";
            this.balanceDataGridViewTextBoxColumn.ReadOnly = true;
            this.balanceDataGridViewTextBoxColumn.Width = 50;
            // 
            // contestCostDataGridViewTextBoxColumn
            // 
            this.contestCostDataGridViewTextBoxColumn.DataPropertyName = "ContestCost";
            this.contestCostDataGridViewTextBoxColumn.HeaderText = "CST";
            this.contestCostDataGridViewTextBoxColumn.Name = "contestCostDataGridViewTextBoxColumn";
            this.contestCostDataGridViewTextBoxColumn.ReadOnly = true;
            this.contestCostDataGridViewTextBoxColumn.Width = 30;
            // 
            // BsAllDKTeams
            // 
            this.BsAllDKTeams.DataSource = typeof(DKFootball.DKTeam);
            // 
            // ClbPositions
            // 
            this.ClbPositions.CheckOnClick = true;
            this.ClbPositions.FormattingEnabled = true;
            this.ClbPositions.Items.AddRange(new object[] {
            "QB",
            "WR",
            "RB",
            "TE",
            "DST"});
            this.ClbPositions.Location = new System.Drawing.Point(887, 41);
            this.ClbPositions.Name = "ClbPositions";
            this.ClbPositions.Size = new System.Drawing.Size(50, 139);
            this.ClbPositions.TabIndex = 3;
            this.ClbPositions.SelectedIndexChanged += new System.EventHandler(this.ClbPositions_SelectedIndexChanged);
            // 
            // BtnSportMode
            // 
            this.BtnSportMode.Location = new System.Drawing.Point(968, 41);
            this.BtnSportMode.Name = "BtnSportMode";
            this.BtnSportMode.Size = new System.Drawing.Size(75, 23);
            this.BtnSportMode.TabIndex = 4;
            this.BtnSportMode.Text = "NFL";
            this.BtnSportMode.UseVisualStyleBackColor = true;
            this.BtnSportMode.Click += new System.EventHandler(this.BtnSportMode_Click);
            // 
            // BtnResults
            // 
            this.BtnResults.Location = new System.Drawing.Point(1049, 12);
            this.BtnResults.Name = "BtnResults";
            this.BtnResults.Size = new System.Drawing.Size(75, 23);
            this.BtnResults.TabIndex = 5;
            this.BtnResults.Text = "RESULTS";
            this.BtnResults.UseVisualStyleBackColor = true;
            this.BtnResults.Click += new System.EventHandler(this.BtnResults_Click);
            // 
            // BtnResultsDetails
            // 
            this.BtnResultsDetails.Location = new System.Drawing.Point(1130, 12);
            this.BtnResultsDetails.Name = "BtnResultsDetails";
            this.BtnResultsDetails.Size = new System.Drawing.Size(75, 23);
            this.BtnResultsDetails.TabIndex = 6;
            this.BtnResultsDetails.Text = "DETAILS";
            this.BtnResultsDetails.UseVisualStyleBackColor = true;
            this.BtnResultsDetails.Click += new System.EventHandler(this.BtnResultsDetails_Click);
            // 
            // TbSearchBox
            // 
            this.TbSearchBox.Location = new System.Drawing.Point(1131, 43);
            this.TbSearchBox.Name = "TbSearchBox";
            this.TbSearchBox.Size = new System.Drawing.Size(156, 20);
            this.TbSearchBox.TabIndex = 7;
            this.TbSearchBox.TextChanged += new System.EventHandler(this.TbSearchBox_TextChanged);
            // 
            // CbOwned
            // 
            this.CbOwned.AutoSize = true;
            this.CbOwned.Location = new System.Drawing.Point(887, 187);
            this.CbOwned.Name = "CbOwned";
            this.CbOwned.Size = new System.Drawing.Size(68, 17);
            this.CbOwned.TabIndex = 9;
            this.CbOwned.Text = "OWNED";
            this.CbOwned.UseVisualStyleBackColor = true;
            this.CbOwned.CheckedChanged += new System.EventHandler(this.CbOwned_CheckedChanged);
            // 
            // BtnSwapPlayers
            // 
            this.BtnSwapPlayers.Location = new System.Drawing.Point(887, 210);
            this.BtnSwapPlayers.Name = "BtnSwapPlayers";
            this.BtnSwapPlayers.Size = new System.Drawing.Size(75, 23);
            this.BtnSwapPlayers.TabIndex = 10;
            this.BtnSwapPlayers.Text = "SWAP";
            this.BtnSwapPlayers.UseVisualStyleBackColor = true;
            this.BtnSwapPlayers.Click += new System.EventHandler(this.BtnSwapPlayers_Click);
            // 
            // BtnExportCSV
            // 
            this.BtnExportCSV.Location = new System.Drawing.Point(1212, 12);
            this.BtnExportCSV.Name = "BtnExportCSV";
            this.BtnExportCSV.Size = new System.Drawing.Size(75, 23);
            this.BtnExportCSV.TabIndex = 11;
            this.BtnExportCSV.Text = "EXPORT";
            this.BtnExportCSV.UseVisualStyleBackColor = true;
            this.BtnExportCSV.Click += new System.EventHandler(this.BtnExportCSV_Click);
            // 
            // BtnSwitchTeams
            // 
            this.BtnSwitchTeams.Location = new System.Drawing.Point(887, 239);
            this.BtnSwitchTeams.Name = "BtnSwitchTeams";
            this.BtnSwitchTeams.Size = new System.Drawing.Size(75, 23);
            this.BtnSwitchTeams.TabIndex = 12;
            this.BtnSwitchTeams.Text = "SWITCHTM";
            this.BtnSwitchTeams.UseVisualStyleBackColor = true;
            this.BtnSwitchTeams.Click += new System.EventHandler(this.BtnSwitchTeams_Click);
            // 
            // CbStarred
            // 
            this.CbStarred.AutoSize = true;
            this.CbStarred.Location = new System.Drawing.Point(968, 187);
            this.CbStarred.Name = "CbStarred";
            this.CbStarred.Size = new System.Drawing.Size(78, 17);
            this.CbStarred.TabIndex = 13;
            this.CbStarred.Text = "STARRED";
            this.CbStarred.UseVisualStyleBackColor = true;
            this.CbStarred.CheckedChanged += new System.EventHandler(this.CbStarred_CheckedChanged);
            // 
            // BtnContestMode
            // 
            this.BtnContestMode.Location = new System.Drawing.Point(968, 12);
            this.BtnContestMode.Name = "BtnContestMode";
            this.BtnContestMode.Size = new System.Drawing.Size(75, 23);
            this.BtnContestMode.TabIndex = 15;
            this.BtnContestMode.Text = "SALARY";
            this.BtnContestMode.UseVisualStyleBackColor = true;
            this.BtnContestMode.Click += new System.EventHandler(this.BtnContestMode_Click);
            // 
            // CbShowCheckedTeams
            // 
            this.CbShowCheckedTeams.AutoSize = true;
            this.CbShowCheckedTeams.Location = new System.Drawing.Point(968, 210);
            this.CbShowCheckedTeams.Name = "CbShowCheckedTeams";
            this.CbShowCheckedTeams.Size = new System.Drawing.Size(75, 17);
            this.CbShowCheckedTeams.TabIndex = 13;
            this.CbShowCheckedTeams.Text = "PLAYERS";
            this.CbShowCheckedTeams.UseVisualStyleBackColor = true;
            this.CbShowCheckedTeams.CheckedChanged += new System.EventHandler(this.CbShowCheckedTeams_CheckedChanged);
            // 
            // BtnSaveState
            // 
            this.BtnSaveState.Location = new System.Drawing.Point(1212, 70);
            this.BtnSaveState.Name = "BtnSaveState";
            this.BtnSaveState.Size = new System.Drawing.Size(75, 23);
            this.BtnSaveState.TabIndex = 18;
            this.BtnSaveState.Text = "SAVE";
            this.BtnSaveState.UseVisualStyleBackColor = true;
            this.BtnSaveState.Click += new System.EventHandler(this.BtnSaveState_Click);
            // 
            // BtnLoadState
            // 
            this.BtnLoadState.Location = new System.Drawing.Point(1212, 99);
            this.BtnLoadState.Name = "BtnLoadState";
            this.BtnLoadState.Size = new System.Drawing.Size(75, 23);
            this.BtnLoadState.TabIndex = 19;
            this.BtnLoadState.Text = "LOAD";
            this.BtnLoadState.UseVisualStyleBackColor = true;
            this.BtnLoadState.Click += new System.EventHandler(this.BtnLoadState_Click);
            // 
            // BtnImportLineupsCSV
            // 
            this.BtnImportLineupsCSV.Location = new System.Drawing.Point(1212, 129);
            this.BtnImportLineupsCSV.Name = "BtnImportLineupsCSV";
            this.BtnImportLineupsCSV.Size = new System.Drawing.Size(75, 23);
            this.BtnImportLineupsCSV.TabIndex = 20;
            this.BtnImportLineupsCSV.Text = "LINEUPS";
            this.BtnImportLineupsCSV.UseVisualStyleBackColor = true;
            this.BtnImportLineupsCSV.Click += new System.EventHandler(this.BtnImportLineupsCSV_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OliveDrab;
            this.ClientSize = new System.Drawing.Size(1299, 601);
            this.Controls.Add(this.BtnImportLineupsCSV);
            this.Controls.Add(this.BtnLoadState);
            this.Controls.Add(this.BtnSaveState);
            this.Controls.Add(this.BtnContestMode);
            this.Controls.Add(this.CbShowCheckedTeams);
            this.Controls.Add(this.CbStarred);
            this.Controls.Add(this.BtnSwitchTeams);
            this.Controls.Add(this.BtnExportCSV);
            this.Controls.Add(this.BtnSwapPlayers);
            this.Controls.Add(this.CbOwned);
            this.Controls.Add(this.TbSearchBox);
            this.Controls.Add(this.BtnResultsDetails);
            this.Controls.Add(this.BtnResults);
            this.Controls.Add(this.BtnSportMode);
            this.Controls.Add(this.ClbPositions);
            this.Controls.Add(this.DgvTeams);
            this.Controls.Add(this.DgvAllPlayers);
            this.Controls.Add(this.BtnOpen);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.DgvAllPlayers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsAllDKPlayers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTeams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsAllDKTeams)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnOpen;
        private System.Windows.Forms.DataGridView DgvAllPlayers;
        private System.Windows.Forms.DataGridView DgvTeams;
        private System.Windows.Forms.BindingSource BsAllDKPlayers;
        private System.Windows.Forms.CheckedListBox ClbPositions;
        private System.Windows.Forms.Button BtnSportMode;
        private System.Windows.Forms.Button BtnResults;
        private System.Windows.Forms.BindingSource BsAllDKTeams;
        private System.Windows.Forms.Button BtnResultsDetails;
        private System.Windows.Forms.TextBox TbSearchBox;
        private System.Windows.Forms.CheckBox CbOwned;
        private System.Windows.Forms.Button BtnSwapPlayers;
        private System.Windows.Forms.Button BtnExportCSV;
        private System.Windows.Forms.Button BtnSwitchTeams;
        private System.Windows.Forms.CheckBox CbStarred;
        private System.Windows.Forms.Button BtnContestMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalTeamsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn moneySpentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PointsScored;
        private System.Windows.Forms.DataGridViewTextBoxColumn PercentOwned;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Owned;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Starred;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ShowTeams;
        private System.Windows.Forms.CheckBox CbShowCheckedTeams;
        private System.Windows.Forms.DataGridViewTextBoxColumn qBDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rB1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rB2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wR1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wR2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wR3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLEXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dSTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn balanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contestCostDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button BtnSaveState;
        private System.Windows.Forms.Button BtnLoadState;
        private System.Windows.Forms.Button BtnImportLineupsCSV;
    }
}


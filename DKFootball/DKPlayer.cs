﻿namespace DKFootball
{
    public class DKPlayer
    {
        private string _name;
        public string Name { get => _name;
            set => _name = value.TrimEnd(' ');
        }
        public string Position { get; set; }
        public int Salary { get; set; }
        public string Id { get; set; }
        public string Team { get; set; }

        public int TotalTeams { get; set; }
        public float MoneySpent { get; set; }

        public float PercentOwned { get; set; }
        public float PointsScored { get; set; }

        public bool Owned => TotalTeams > 0;
        public bool TeamCreationMode = true; //need to remove or figure out temporary fix
        
        public bool Starred { get; set; } = false;
        public bool ShowTeams { get; set; } = false;

        public DKPlayer()
        {
        }

        public string StringForCSV()
        {
            return Name + " (" + Id + ") ";
        }

        public override string ToString()
        {
            if (MainForm.RegularNameMode)
            {
                return Name + " " + Salary.ToString();
            }
            else
            {
                return Name + " " + PercentOwned.ToString() + "% " + PointsScored.ToString();
            }
        }
    }
}

﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Drawing;

namespace DKFootball
{
    public partial class MainForm : Form
    {
        private const String DownloadDirectory = @"c:\users\demei\downloads\DKFolders";
        private String LineupBeforeFolderName => SalaryModeOn ?
            (BaseSportModeOn ? "DK NFL Entries SAL" : "DK NBA Entries SAL") : (BaseSportModeOn? "DK NFL Entries PEM" : "DK NBA Entries PEM");
        private String ResultsFolderName => SalaryModeOn ?
            (BaseSportModeOn ? "DK NFL Results SAL" : "DK NBA Results SAL") : (BaseSportModeOn ? "DK NFL Results PEM" : "DK NBA Results PEM");
        private String SavedStateFolderName => "DKSaved";
        private String ExportedFolderName => "DKExport";
        private String RotoGrindersLineupsFolderName => "RGLineups";
        private String ExportFileName => SalaryModeOn ? 
            (BaseSportModeOn ? "DKNFLExportSAL.csv" : "DKNBAExportSAL.csv") : (BaseSportModeOn ? "DKNFLExportPEM.csv" : "DKNBAExportPEM.csv");
        private String SavedStateFileName => SalaryModeOn ? 
            (BaseSportModeOn ? "DKNFLSavedStateSAL.csv" : "DKNBASavedStateSAL.csv") : (BaseSportModeOn? "DKNFLSavedStatePEM.csv" : "DKNBASavedStatePEM.csv");
        private String StarredPlayersFileName => SalaryModeOn ? 
            (BaseSportModeOn? "DKNFLStarredPlayersSAL.csv" : "DKNBAStarredPlayersSAL.csv") : (BaseSportModeOn? "DKNFLStarredPlayersSAL.csv" : "DKNBAStarredPlayersSAL.csv");

        private String CsvOutputFirstLine { get; set; }

        private List<DKPlayer> AllPlayers { get; set; }
        private List<DKTeam> UserCreatedTeams { get; set; }
        private SortableBindingList<DKTeam> ResultTeams { get; set; }

        public static bool RegularNameMode { get; set; } = true;

        private bool SalaryModeOn => BtnContestMode.Text == "SALARY";
        private bool BaseSportModeOn => BtnSportMode.Text == "NFL";

        private DKTeam CopiedTeam { get; set; }

        public MainForm() 
        {
            InitializeComponent();
        }

        private void NFLSalaryPlayerReader(String fileName)
        {
            StreamReader playersLF = new StreamReader(fileName);
            for (int i = 0; i < 8; i++)
            { playersLF.ReadLine(); }

            string playerInfo;
            while (!playersLF.EndOfStream)
            {
                playerInfo = playersLF.ReadLine();
                string[] playerInfoWords = playerInfo.Split(new char[] { ',' });
                DKPlayer player = new DKPlayer
                {
                    Name = playerInfoWords[16],
                    Position = playerInfoWords[14],
                    Salary = int.Parse(playerInfoWords[18]),
                    Team = playerInfoWords[20],
                    Id = playerInfoWords[17]
                };
                BsAllDKPlayers.Add(player);
            }

            AllPlayers = ((SortableBindingList<DKPlayer>)BsAllDKPlayers.DataSource).ToList();
            
            playersLF.Dispose();
        }

        private void NFLSalaryTeamReader(String fileName)
        {
            StreamReader teamsLF = new StreamReader(fileName);
            CsvOutputFirstLine = teamsLF.ReadLine();        
            string teamInfo;
            while (!teamsLF.EndOfStream)
            {
                teamInfo = teamsLF.ReadLine();
                
                string[] commaSeperatedWords = teamInfo.Split(new char[] { ',' });
                string currentTeamPrice = commaSeperatedWords[3];

                currentTeamPrice = currentTeamPrice.TrimStart('$');
                if (currentTeamPrice == "")
                { break; }

                DKTeam newTeam = new DKTeam
                {
                    ContestCost = float.Parse(currentTeamPrice),
                    ContestID = int.Parse(commaSeperatedWords[2]),
                    EntryID = int.Parse(commaSeperatedWords[0])
                };
                UserCreatedTeams.Add(newTeam);

                for (int i = 4; i <= 12; i++)
                {
                    string playerName = commaSeperatedWords[i];
                    string[] player1Words = playerName.Split(new char[] { '(', ')' });

                    newTeam.AddPlayerToTeam(AllPlayers.Find(x => x.Id == player1Words[1]));
                }
            }
            teamsLF.Dispose();
        }

        private void NFLPickemPlayerReader(String fileName)
        {
            StreamReader playersLF = new StreamReader(fileName);

            string[] split = playersLF.ReadLine().Split(new char[] { ',' });
            int instructionsColumnNumber = split.ToList().IndexOf("Instructions");

            for (int i = 0; i < 7; i++)
            { playersLF.ReadLine(); }

            string playerInfo;
            while (!playersLF.EndOfStream)
            {
                playerInfo = playersLF.ReadLine();
                string[] playerInfoWords = playerInfo.Split(new char[] { ',' });
                if (playerInfoWords.Count() < instructionsColumnNumber+2)
                { break; }
                DKPlayer player = new DKPlayer
                {
                    Name = playerInfoWords[instructionsColumnNumber + 2],
                    Position = playerInfoWords[instructionsColumnNumber + 4],
                    Team = playerInfoWords[instructionsColumnNumber + 6],
                    Id = playerInfoWords[instructionsColumnNumber + 3]
                };
                BsAllDKPlayers.Add(player);
            }

            AllPlayers = ((SortableBindingList<DKPlayer>)BsAllDKPlayers.DataSource).ToList();
            
            playersLF.Dispose();
        }

        private void NFLPickemTeamReader(String fileName)
        {
            StreamReader teamsLF = new StreamReader(fileName);
            CsvOutputFirstLine = teamsLF.ReadLine();
            string[] split = CsvOutputFirstLine.Split(new char[] { ',' });
            int instructionsColumnNumber = split.ToList().IndexOf("Instructions");
            Console.WriteLine(" dfadf " + instructionsColumnNumber.ToString());

            string teamInfo;
            while (!teamsLF.EndOfStream)
            {
                teamInfo = teamsLF.ReadLine();
                
                string[] commaSeperatedWords = teamInfo.Split(new char[] { ',' });
                string currentTeamPrice = commaSeperatedWords[3];


                currentTeamPrice = currentTeamPrice.TrimStart('$');
                if (currentTeamPrice == "")
                    { break; }

                DKTeam newTeam = new DKTeam
                {
                    ContestCost = float.Parse(currentTeamPrice),
                    ContestID = int.Parse(commaSeperatedWords[2]),
                    EntryID = int.Parse(commaSeperatedWords[0])
                };
                UserCreatedTeams.Add(newTeam);

                for (int i = 4; i <= instructionsColumnNumber-2; i++)
                {
                    string playerName = commaSeperatedWords[i];
                    string[] player1Words = playerName.Split(new char[] { '(', ')' });

                    newTeam.AddPlayerToTeam(AllPlayers.Find(x => x.Id == player1Words[1]));
                }
            }
            
            teamsLF.Dispose();
        }
        
        private void NBASalaryPlayerReader(String fileName)
        {
            StreamReader playersLF = new StreamReader(fileName);
            for (int i = 0; i < 8; i++)
            { playersLF.ReadLine(); }

            string playerInfo;
            while (!playersLF.EndOfStream)
            {
                playerInfo = playersLF.ReadLine();
                string[] playerInfoWords = playerInfo.Split(new char[] { ',' });
                DKPlayer player = new DKPlayer
                {
                    Name = playerInfoWords[15],
                    Position = playerInfoWords[13],
                    Salary = int.Parse(playerInfoWords[17]),
                    Team = playerInfoWords[19],
                    Id = playerInfoWords[16]
                };
                BsAllDKPlayers.Add(player);
            }

            AllPlayers = ((SortableBindingList<DKPlayer>)BsAllDKPlayers.DataSource).ToList();

            playersLF.Dispose();
        }

        private void NBASalaryTeamReader(String fileName)
        {
            StreamReader teamsLF = new StreamReader(fileName);
            CsvOutputFirstLine = teamsLF.ReadLine();
            string teamInfo;
            while (!teamsLF.EndOfStream)
            {
                teamInfo = teamsLF.ReadLine();

                string[] commaSeperatedWords = teamInfo.Split(new char[] { ',' });
                string currentTeamPrice = commaSeperatedWords[3];

                currentTeamPrice = currentTeamPrice.TrimStart('$');
                if (currentTeamPrice == "")
                { break; }

                DKTeam newTeam = new DKTeam
                {
                    ContestCost = float.Parse(currentTeamPrice),
                    ContestID = int.Parse(commaSeperatedWords[2]),
                    EntryID = int.Parse(commaSeperatedWords[0])
                };
                UserCreatedTeams.Add(newTeam);

                for (int i = 4; i <= 11; i++)
                {
                    string playerName = commaSeperatedWords[i];
                    string[] player1Words = playerName.Split(new char[] { '(', ')' });

                    newTeam.AddNBAPlayerToTeam(AllPlayers.Find(x => x.Id == player1Words[1]), i-4);
                }
            }
            teamsLF.Dispose();
        }

        private void ResultsReader(String fileName)
        {
            StreamReader playerListFile = new StreamReader(fileName);
            ResultTeams = new SortableBindingList<DKTeam>();
            string playerInfo;
            playerListFile.ReadLine();

            while (!playerListFile.EndOfStream)
            {
                playerInfo = playerListFile.ReadLine();
                string[] words = playerInfo.Split(new char[] { ',' });

                if (words[7] != "")
                {
                    int counter = 0;
                    string[] words2 = words[8].Split(new char[] { '%' });

                    foreach (DKPlayer plyr in AllPlayers.Where(x => x.Name == words[7].TrimEnd(' ')))
                    {
                        plyr.PointsScored = float.Parse(words[9]);
                        plyr.PercentOwned = float.Parse(words2[0]);
                        counter++;
                    }

                    if (counter != 1)
                        { Console.WriteLine(words[7]); } //check if more than 1 player have the same name 
                }

                DKTeam newTeam = new DKTeam();
                ResultTeams.Add(newTeam);

                if (words[4] != "")
                {
                    newTeam.TeamPoints = float.Parse(words[4]);
                    newTeam.Placed = ResultTeams.Count;
                }

                if (words[5] != "")
                {
                    string[] allPlayersOnTeam = words[5].Split(new char[] { ' ' });

                    string position = "";
                    string playerName = "";

                    foreach (String st in allPlayersOnTeam)
                    {
                        if (st == "QB" || st == "RB" || st == "WR" || st == "TE" || st == "DST" || st == "FLEX")
                        {
                            if (position != "" && playerName != "")
                            {
                                newTeam.AddPlayerToTeam(AllPlayers.Find(x => x.Name == playerName.TrimEnd(' ')));
                                playerName = "";
                            }

                            position = st;
                        }
                        else if (st != "")
                        {
                            if (playerName == "")
                                { playerName = st; }
                            else
                                { playerName += " " + st; }
                        }
                        else
                            { playerName += " "; }
                    }

                    if (position != "" && playerName != "")
                        { newTeam.AddPlayerToTeam(AllPlayers.Find(x => x.Name == playerName.TrimEnd(' '))); }
                }

                if (ResultTeams.Count > 10000)
                    { break; }
            }
            playerListFile.Dispose();
        }

        private void ApplyFilter()
        {
            string filterString = "";
            if (CbStarred.Checked)
            {
                filterString += String.Format("Starred = 'true'") + Environment.NewLine;
            }
            if (CbOwned.Checked)
            {
                filterString += String.Format("Owned = 'true'") + Environment.NewLine;
            }

            string res1 = "";
            if (ClbPositions.CheckedIndices.Count == 1)
            {
                res1 += ClbPositions.CheckedItems[0].ToString();
                filterString += String.Format("Position %% '{0}'", res1) + Environment.NewLine;
            }
            else if(ClbPositions.CheckedIndices.Count > 1)
            {
                foreach (var item in ClbPositions.CheckedItems)
                {
                    res1 += item.ToString() + "||";
                }
                filterString += String.Format("Position = '{0}'", res1) + Environment.NewLine;

            }

            if (TbSearchBox.Text != "")
            {
                filterString += String.Format("Name %% '{0}'", TbSearchBox.Text);
            }

            BsAllDKPlayers.RemoveFilter();
            BsAllDKPlayers.Filter = filterString;

            DgvAllPlayers.Refresh();
        }

        private void UpdateCheckListBoxes()
        {
            ClbPositions.Items.Clear();
            if (SalaryModeOn && BaseSportModeOn)
            {
                while (ClbPositions.Items.Count != 5)
                { ClbPositions.Items.Add(""); }
                ClbPositions.Items[0] = "QB";
                ClbPositions.Items[1] = "RB";
                ClbPositions.Items[2] = "WR";
                ClbPositions.Items[3] = "TE";
                ClbPositions.Items[4] = "DST";

                for (int i = 0; i <= 8; i++)
                {
                    switch (i)
                    {
                        case 0:
                            DgvTeams.Columns[i].DataPropertyName = "QB";
                            break;
                        case 1:
                            DgvTeams.Columns[i].DataPropertyName = "RB1";
                            break;
                        case 2:
                            DgvTeams.Columns[i].DataPropertyName = "RB2";
                            break;
                        case 3:
                            DgvTeams.Columns[i].DataPropertyName = "WR1";
                            break;
                        case 4:
                            DgvTeams.Columns[i].DataPropertyName = "WR2";
                            break;
                        case 5:
                            DgvTeams.Columns[i].DataPropertyName = "WR3";
                            break;
                        case 6:
                            DgvTeams.Columns[i].DataPropertyName = "TE";
                            break;
                        case 7:
                            DgvTeams.Columns[i].DataPropertyName = "FLEX";
                            break;
                        case 8:
                            DgvTeams.Columns[i].DataPropertyName = "DST";
                            break;
                        default:
                            break;
                    }
                    DgvTeams.Columns[i].HeaderText = DgvTeams.Columns[i].DataPropertyName;
                }
            }
            else if (SalaryModeOn && !BaseSportModeOn)
            {
                while (ClbPositions.Items.Count != 7)
                { ClbPositions.Items.Add(""); }
                ClbPositions.Items[0] = "PG";
                ClbPositions.Items[1] = "SG";
                ClbPositions.Items[2] = "SF";
                ClbPositions.Items[3] = "PF";
                ClbPositions.Items[4] = "C";
                ClbPositions.Items[5] = "G";
                ClbPositions.Items[6] = "F";
                for (int i = 0; i <= 8; i++)
                {
                    switch (i)
                    {
                        case 0:
                            DgvTeams.Columns[i].DataPropertyName = "PG";
                            break;
                        case 1:
                            DgvTeams.Columns[i].DataPropertyName = "SG";
                            break;
                        case 2:
                            DgvTeams.Columns[i].DataPropertyName = "SF";
                            break;
                        case 3:
                            DgvTeams.Columns[i].DataPropertyName = "PF";
                            break;
                        case 4:
                            DgvTeams.Columns[i].DataPropertyName = "C";
                            break;
                        case 5:
                            DgvTeams.Columns[i].DataPropertyName = "G";
                            break;
                        case 6:
                            DgvTeams.Columns[i].DataPropertyName = "F";
                            break;
                        case 7:
                            DgvTeams.Columns[i].DataPropertyName = "UTIL";
                            break;
                        case 8:
                            DgvTeams.Columns[i].DataPropertyName = "";
                            break;
                        default:
                            break;
                    }
                    DgvTeams.Columns[i].HeaderText = DgvTeams.Columns[i].DataPropertyName;
                }

            }
            else
            {
                ClbPositions.Items.Clear();
                while (ClbPositions.Items.Count != 8)
                { ClbPositions.Items.Add(""); }
                ClbPositions.Items[0] = "T1";
                ClbPositions.Items[1] = "T2";
                ClbPositions.Items[2] = "T3";
                ClbPositions.Items[3] = "T4";
                ClbPositions.Items[4] = "T5";
                ClbPositions.Items[5] = "T6";
                ClbPositions.Items[6] = "T7";
                ClbPositions.Items[7] = "T8";

                for (int i = 0; i < 9; i++)
                {
                    switch (i)
                    {
                        case 0:
                            DgvTeams.Columns[i].DataPropertyName = "T1";
                            break;
                        case 1:
                            DgvTeams.Columns[i].DataPropertyName = "T2";
                            break;
                        case 2:
                            DgvTeams.Columns[i].DataPropertyName = "T3";
                            break;
                        case 3:
                            DgvTeams.Columns[i].DataPropertyName = "T4";
                            break;
                        case 4:
                            DgvTeams.Columns[i].DataPropertyName = "T5";
                            break;
                        case 5:
                            DgvTeams.Columns[i].DataPropertyName = "T6";
                            break;
                        case 6:
                            DgvTeams.Columns[i].DataPropertyName = "T7";
                            break;
                        case 7:
                            DgvTeams.Columns[i].DataPropertyName = "T8";
                            break;
                        case 8:
                            DgvTeams.Columns[i].DataPropertyName = "";
                            break;
                        default:
                            break;
                    }
                    DgvTeams.Columns[i].HeaderText = DgvTeams.Columns[i].DataPropertyName;
                }
            }
        }

        private void BtnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog { InitialDirectory = DownloadDirectory + @"\" + LineupBeforeFolderName};
            openFile.Filter = "CSV File (*.csv)|*.csv";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                BsAllDKPlayers.DataSource = new SortableBindingList<DKPlayer>();
                BsAllDKPlayers.RemoveFilter();
                UserCreatedTeams = new List<DKTeam>();

                if (SalaryModeOn && BaseSportModeOn)
                {
                    NFLSalaryPlayerReader(openFile.FileName);
                    NFLSalaryTeamReader(openFile.FileName);
                }
                else if (!SalaryModeOn && BaseSportModeOn)
                {
                    NFLPickemPlayerReader(openFile.FileName);
                    NFLPickemTeamReader(openFile.FileName);
                }
                else if (SalaryModeOn && !BaseSportModeOn)
                {
                    NBASalaryPlayerReader(openFile.FileName);
                    NBASalaryTeamReader(openFile.FileName);
                }
                BtnContestMode.Enabled = false;
                BtnSportMode.Enabled = false;

                BsAllDKTeams.DataSource = new SortableBindingList<DKTeam>(UserCreatedTeams);
            }
        }

        private void BtnImportLineupsCSV_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog { InitialDirectory = DownloadDirectory + @"\" + RotoGrindersLineupsFolderName };
            openFile.Filter = "CSV File (*.csv)|*.csv";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                StreamReader rgLineupsReader = new StreamReader(openFile.FileName);

                rgLineupsReader.ReadLine();

                string teamInfo;

                int counter = 0;
                while (!rgLineupsReader.EndOfStream && counter < DgvTeams.SelectedRows.Count)
                {
                    teamInfo = rgLineupsReader.ReadLine();

                    DKTeam workingTeam = (DKTeam)DgvTeams.SelectedRows[counter].DataBoundItem;
                    DKTeam newTeam = new DKTeam();
                    newTeam.ContestID = workingTeam.ContestID;
                    newTeam.EntryID = workingTeam.EntryID;
                    newTeam.ContestCost = workingTeam.ContestCost;
                    

                    List<DKPlayer> playersToSwap = new List<DKPlayer>();

                    string[] commaSeperatedWords = teamInfo.Split(new char[] { ',' });
                    for (int i = 0; i < commaSeperatedWords.Count(); i++)
                    {
                        string playerName = commaSeperatedWords[i];
                        string[] player1Words = playerName.Split(new char[] { '(', ')' });

                        DKPlayer playerToAdd = AllPlayers.Find(x => x.Id == player1Words[1]);

                        if (BaseSportModeOn)
                        {
                            newTeam.AddPlayerToTeam(playerToAdd);
                        }
                        else
                        {
                            newTeam.AddNBAPlayerToTeam(playerToAdd, i);
                        }
                    }
                    SortableBindingList<DKTeam> tm = (SortableBindingList<DKTeam>)BsAllDKTeams.DataSource;
                    tm[tm.IndexOf((DKTeam)DgvTeams.SelectedRows[counter].DataBoundItem)] = newTeam;
                    
                    counter++;
                }

                rgLineupsReader.Dispose();
                DgvAllPlayers.Refresh();
                DgvTeams.Refresh();
            }
        }
        
        private void BtnResults_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog{ InitialDirectory = DownloadDirectory + @"\" + ResultsFolderName }; 
            openFile.Filter = "CSV File (*.csv)|*.csv";

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                ResultsReader(openFile.FileName);
                DgvAllPlayers.Refresh();
            }
        }
        
        private void BtnSwapPlayers_Click(object sender, EventArgs e)
        {
            DKPlayer selectedPlayer = (DKPlayer)DgvAllPlayers.CurrentRow.DataBoundItem;
            DKPlayer teamPlayer = (DKPlayer)DgvTeams.CurrentCell.Value;
            DKTeam team = (DKTeam)DgvTeams.CurrentRow.DataBoundItem;

            team.SwapPlayers(teamPlayer, selectedPlayer);

            DgvTeams.Refresh();
            DgvAllPlayers.Refresh();
        }

        private void BtnExportCSV_Click(object sender, EventArgs e)
        {
            StreamWriter outputStream = new StreamWriter(DownloadDirectory + @"\" + ExportedFolderName + @"\" + ExportFileName);
            outputStream.WriteLine(CsvOutputFirstLine);
            foreach (DKTeam team in (SortableBindingList<DKTeam>)BsAllDKTeams.DataSource)
            {
                string output = SalaryModeOn ? (BaseSportModeOn ? team.NFLSalaryExportString() : team.NBASalaryExportString()) : team.AllPickemExportString();
                outputStream.WriteLine(output);
            }
            outputStream.Dispose();
        }

        private void BtnSaveState_Click(object sender, EventArgs e)
        {
            StreamWriter outputStream = new StreamWriter(DownloadDirectory + @"\" + SavedStateFolderName + @"\" + SavedStateFileName);
            outputStream.WriteLine(CsvOutputFirstLine);
            foreach (DKTeam team in (SortableBindingList<DKTeam>)BsAllDKTeams.DataSource)
            {
                string output = SalaryModeOn ? (BaseSportModeOn ? team.NFLSalaryExportString() : team.NBASalaryExportString()) : team.AllPickemExportString();
                outputStream.WriteLine(output);
            }
            outputStream.Dispose();

            StreamWriter starredPlayersStream = new StreamWriter(DownloadDirectory + @"\" + SavedStateFolderName + @"\" + StarredPlayersFileName);

            foreach (DKPlayer starred in AllPlayers.Where(x=> x.Starred == true))
            {
                starredPlayersStream.WriteLine(starred.Id);
            }
            starredPlayersStream.Dispose();
            
        }

        private void BtnLoadState_Click(object sender, EventArgs e)
        {
            UserCreatedTeams = new List<DKTeam>();

            foreach (DKPlayer playrr in AllPlayers)
            {
                playrr.MoneySpent = 0;
                playrr.TotalTeams = 0;
            }

            if (SalaryModeOn)
            {
                if (BaseSportModeOn)
                {
                    NFLSalaryTeamReader(DownloadDirectory + @"\" + SavedStateFolderName + @"\" + SavedStateFileName);
                }
                else
                {
                    NBASalaryTeamReader(DownloadDirectory + @"\" + SavedStateFolderName + @"\" + SavedStateFileName);
                }
            }
            else
                { NFLPickemTeamReader(DownloadDirectory + @"\" + SavedStateFolderName + @"\" + SavedStateFileName); }
            BsAllDKTeams.DataSource = new SortableBindingList<DKTeam>(UserCreatedTeams);

            StreamReader starredPlayersReader = new StreamReader(DownloadDirectory + @"\" + SavedStateFolderName + @"\" + StarredPlayersFileName);
            while (!starredPlayersReader.EndOfStream)
            {
                string plyrID = starredPlayersReader.ReadLine();
                DKPlayer plyr = AllPlayers.Find(x => x.Id == plyrID);
                plyr.Starred = true;
            }


            starredPlayersReader.Dispose();
            DgvAllPlayers.Refresh();
            DgvTeams.Refresh();
        }

        private void BtnSwitchTeams_Click(object sender, EventArgs e)
        {
            if (DgvTeams.SelectedRows.Count == 2)
            {
                DKTeam team = (DKTeam)DgvTeams.SelectedRows[0].DataBoundItem;
                team.SwitchTeamsEntries((DKTeam)DgvTeams.SelectedRows[1].DataBoundItem);
            }

            DgvTeams.Refresh();
        }
        
        private void BtnContestMode_Click(object sender, EventArgs e)
        {
            if (BtnContestMode.Text == "SALARY")
                { BtnContestMode.Text = "PICKEM"; }
            else if (BtnContestMode.Text == "PICKEM")
                { BtnContestMode.Text = "SALARY"; }

            UpdateCheckListBoxes();
        }

        private void BtnSportMode_Click(object sender, EventArgs e)
        {
            if (BtnSportMode.Text == "NFL")
            { BtnSportMode.Text = "NBA"; }
            else if (BtnSportMode.Text == "NBA")
            { BtnSportMode.Text = "NFL"; }

            UpdateCheckListBoxes();
        }

        private void BtnResultsDetails_Click(object sender, EventArgs e)
        {
            DetailedResultsForm FrmOptions = new DetailedResultsForm();
            FrmOptions.Teams(ResultTeams);
            RegularNameMode = false;
            FrmOptions.Show();
        }
        
        private void CbOwned_CheckedChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }

        private void CbStarred_CheckedChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }

        private void ClbPositions_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }
        
        private void CbShowCheckedTeams_CheckedChanged(object sender, EventArgs e)
        {
            if (CbShowCheckedTeams.Checked)
            {
                BsAllDKTeams.RemoveFilter();
                BsAllDKTeams.Filter = String.Format("TeamContainsAShownPlayer = 'true'");
            }
            else
                {
                BsAllDKTeams.RemoveFilter(); 
            }

            DgvTeams.Refresh();
        }

        private void DgvTeams_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DKPlayer player = (DKPlayer)DgvTeams.CurrentCell.Value;
            int playerIndex = BsAllDKPlayers.IndexOf((DKPlayer)DgvTeams.CurrentCell.Value);
            DgvAllPlayers.FirstDisplayedScrollingRowIndex = playerIndex;
            DgvAllPlayers.Rows[playerIndex].Selected = true;
        }

        private void DgvAllPlayers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DetailedPlayerForm plyrForm = new DetailedPlayerForm();
            DKPlayer selectedPlayer = (DKPlayer)DgvAllPlayers.CurrentRow.DataBoundItem;
            plyrForm.Text = selectedPlayer.Name;
            plyrForm.Show();
        }

        private void TbSearchBox_TextChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.C))
            {
                if (DgvTeams.SelectedRows.Count == 1)
                {
                    DataGridViewRow row = DgvTeams.SelectedRows[0];
                    row.DefaultCellStyle.BackColor = Color.Red;
                    row.Selected = false;
                    CopiedTeam = (DKTeam)row.DataBoundItem;
                }
            }
            else if (e.KeyData == (Keys.Control | Keys.V))
            {
                if (CopiedTeam != null)
                {
                    DataGridViewRow row = DgvTeams.Rows[BsAllDKTeams.IndexOf(CopiedTeam)];
                    row.DefaultCellStyle = DgvTeams.RowsDefaultCellStyle;
                    foreach (DataGridViewRow rw in DgvTeams.SelectedRows)
                    {
                        
                        DKTeam selTeam = (DKTeam)rw.DataBoundItem;
                        selTeam.CopyTeam(CopiedTeam);
                    
                    }
                    CopiedTeam = null;
                    DgvTeams.Refresh();
                    DgvAllPlayers.Refresh();
                }
            }
        }

    }
}

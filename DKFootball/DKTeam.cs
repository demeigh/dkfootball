﻿using System;
using System.Collections.Generic;

namespace DKFootball
{
    public class DKTeam
    {
        public List<DKPlayer> _rbs = new List<DKPlayer>();
        public List<DKPlayer> _wrs = new List<DKPlayer>();
        public List<DKPlayer> _tes = new List<DKPlayer>();
        public DKPlayer _qb;
        public DKPlayer _dst;

        public DKPlayer blankPlayer = new DKPlayer();

        public DKPlayer QB => _qb;
        public DKPlayer DST => _dst;
        public DKPlayer RB1 => _rbs[0];
        public DKPlayer RB2 => _rbs[1];
        public DKPlayer WR1 => _wrs[0];
        public DKPlayer WR2 => _wrs[1];
        public DKPlayer WR3 => _wrs[2];
        public DKPlayer TE => _tes[0];

        public List<DKPlayer> _tiers = new List<DKPlayer>();
        public DKPlayer T1 => _tiers[0];
        public DKPlayer T2 => _tiers[1];
        public DKPlayer T3 => _tiers[2];
        public DKPlayer T4 => _tiers[3];
        public DKPlayer T5 => _tiers[4];
        public DKPlayer T6 => _tiers[5];
        public DKPlayer T7 => _tiers.Count > 6 ? _tiers[6] : blankPlayer;
        public DKPlayer T8 => _tiers.Count > 7 ? _tiers[7] : blankPlayer;

        public List<DKPlayer> _pgs = new List<DKPlayer>();
        public List<DKPlayer> _sgs = new List<DKPlayer>();
        public List<DKPlayer> _sfs = new List<DKPlayer>();
        public List<DKPlayer> _pfs = new List<DKPlayer>();
        public List<DKPlayer> _cs = new List<DKPlayer>();

        public DKPlayer PG => _pgs[0];
        public DKPlayer SG => _sgs[0];
        public DKPlayer SF => _sfs[0];
        public DKPlayer PF => _pfs[0];
        public DKPlayer C => _cs[0];
        public DKPlayer G
        {
            get
            {
                if (_pgs.Count >= 2)
                { return _pgs[1]; }
                else if (_sgs.Count >= 2)
                { return _sgs[1]; }
                else
                { return new DKPlayer(); }
            }
        }
        public DKPlayer F
        {
            get
            {
                if (_sfs.Count >= 2)
                { return _sfs[1]; }
                else if (_pfs.Count >= 2)
                { return _pfs[1]; }
                else
                { return new DKPlayer(); }
            }
        }
        public DKPlayer UTIL
        {
            get
            {
                if (_pgs.Count == 3)
                { return _pgs[2]; }
                else if (_sgs.Count == 3)
                { return _sgs[2]; }
                else if (_sfs.Count == 3)
                { return _sfs[2]; }
                else if (_pfs.Count == 3)
                { return _pfs[2]; }
                else if (_cs.Count == 2)
                { return _cs[1]; }
                else if (_pgs.Count == 2 && _sgs.Count == 2)
                { return _sgs[1]; }
                else if (_sfs.Count == 2 && _pfs.Count == 2)
                { return _pfs[1]; }
                else
                { return new DKPlayer(); }
            }
        }

        public DKPlayer FLEX
        {
            get
            {
                if (_rbs.Count == 3)
                { return _rbs[2]; }
                else if (_wrs.Count == 4)
                { return _wrs[3]; }
                else if (_tes.Count == 2)
                { return _tes[1]; }
                else
                { return new DKPlayer(); }
            }
        }

        public List<DKPlayer> AllPlayers
        {
            get
            {
                if (_rbs.Count > 0)
                {
                    List<DKPlayer> result = new List<DKPlayer>(_rbs);
                    result.AddRange(_wrs);
                    result.AddRange(_tes);
                    result.Add(_qb);
                    result.Add(_dst);
                    return result;
                }
                else if (_pgs.Count != 0)
                {
                    List<DKPlayer> result = new List<DKPlayer>(_pgs);
                    result.AddRange(_sgs);
                    result.AddRange(_sfs);
                    result.AddRange(_pfs);
                    result.AddRange(_cs);
                    return result;

                }
                else
                { return _tiers; }
            }
        }

        public float TeamPoints { get; set; }
        public int Placed { get; set; }

        public int TotalCost { get; set; }
        public int Balance { get => 50000 - TotalCost; }

        public int ContestID { get; set; }
        public float ContestCost { get; set; }
        public int EntryID { get; set; }

        public void SwitchTeamsEntries(DKTeam switchedTeam)
        {
            int tempCID = ContestID;
            float tempCC = ContestCost;
            int tempEID = EntryID;

            ContestID = switchedTeam.ContestID;
            ContestCost = switchedTeam.ContestCost;
            EntryID = switchedTeam.EntryID;

            switchedTeam.ContestID = tempCID;
            switchedTeam.ContestCost = tempCC;
            switchedTeam.EntryID = tempEID;
        }

        public void CopyTeam(DKTeam team)
        {
            if (_tiers.Count > 0)
            {
                SwapPlayers(T1, team.T1);
                SwapPlayers(T2, team.T2);
                SwapPlayers(T3, team.T3);
                SwapPlayers(T4, team.T4);
                SwapPlayers(T5, team.T5);
                SwapPlayers(T6, team.T6);
                SwapPlayers(T7, team.T7);
                SwapPlayers(T8, team.T8);
            }
            else if (_rbs.Count > 0)
            {
                SwapPlayers(QB, team.QB);
                if (!_rbs.Contains(team.RB1)) { SwapPlayers(RB1, team.RB1); }
                else { _rbs.Remove(team.RB1); _rbs.Insert(0, team.RB1); }
                if (!_rbs.Contains(team.RB2)) { SwapPlayers(RB2, team.RB2); }
                else { _rbs.Remove(team.RB2); _rbs.Insert(1, team.RB2); }
                if (!_wrs.Contains(team.WR1)) { SwapPlayers(WR1, team.WR1); }
                else { _wrs.Remove(team.WR1); _wrs.Insert(0, team.WR1); }
                if (!_wrs.Contains(team.WR2)) { SwapPlayers(WR2, team.WR2); }
                else { _wrs.Remove(team.WR2); _wrs.Insert(1, team.WR2); }
                if (!_wrs.Contains(team.WR3)) { SwapPlayers(WR3, team.WR3); }
                else { _wrs.Remove(team.WR3); _wrs.Insert(2, team.WR3); }
                if (!_tes.Contains(team.TE)) { SwapPlayers(TE, team.TE); }
                else { _tes.Remove(team.TE); _tes.Insert(0, team.TE); }
                if (!_rbs.Contains(team.FLEX) && !_wrs.Contains(team.FLEX) && !_tes.Contains(team.FLEX)) { SwapPlayers(FLEX, team.FLEX); }
                SwapPlayers(DST, team.DST);
            }
        }

        public void AddPlayerToTeam(DKPlayer plyr)
        {
            plyr.TotalTeams++;
            plyr.MoneySpent += ContestCost;

            TotalCost += plyr.Salary;

            if (plyr.Position == "QB")
            {
                _qb = plyr;
            }
            else if (plyr.Position == "DST")
            {
                _dst = plyr;
            }
            else if (plyr.Position == "RB")
            {
                _rbs.Add(plyr);
            }
            else if (plyr.Position == "WR")
            {
                _wrs.Add(plyr);
            }
            else if (plyr.Position == "TE")
            {
                _tes.Add(plyr);
            }
            else
            {
                _tiers.Add(plyr);
            }
        }

        public void AddNBAPlayerToTeam(DKPlayer plyr, int location)
        {
            plyr.TotalTeams++;
            plyr.MoneySpent += ContestCost;

            TotalCost += plyr.Salary;

            switch (location)
            {
                case 0:
                    _pgs.Add(plyr);
                    break;
                case 1:
                    _sgs.Add(plyr);
                    break;
                case 2:
                    _sfs.Add(plyr);
                    break;
                case 3:
                    _pfs.Add(plyr);
                    break;
                case 4:
                    _cs.Add(plyr);
                    break;
                case 5:
                    if (plyr.Position.Contains("PG"))
                    {
                        _pgs.Add(plyr);
                    }
                    else
                    {
                        _sgs.Add(plyr);
                    }
                    break;
                case 6:
                    if (plyr.Position.Contains("SF"))
                    {
                        _sfs.Add(plyr);
                    }
                    else
                    {
                        _pfs.Add(plyr);
                    }
                    break;
                case 7:
                    if (plyr.Position.Contains("PG"))
                    {
                        _pgs.Add(plyr);
                    }
                    else if (plyr.Position.Contains("SG"))
                    {
                        _sgs.Add(plyr);
                    }
                    else if (plyr.Position.Contains("SF"))
                    {
                        _sfs.Add(plyr);
                    }
                    else if (plyr.Position.Contains("PF"))
                    {
                        _pfs.Add(plyr);
                    }
                    else if (plyr.Position.Contains("C"))
                    {
                        _cs.Add(plyr);
                    }
                    break;


                default:
                    break;
            }

        }

        public bool ContainsPlayer(DKPlayer plyr)
        {
            bool results = false;

            if (QB == plyr || DST == plyr || _rbs.Contains(plyr) || _wrs.Contains(plyr) || _tes.Contains(plyr) || _tiers.Contains(plyr) ||
                _pgs.Contains(plyr) || _sgs.Contains(plyr) || _sfs.Contains(plyr) || _pfs.Contains(plyr) || _cs.Contains(plyr))
            {
                results = true;
            }

            return results;
        }

        public bool SwapPlayers(DKPlayer currentPlyr, DKPlayer newPlyr)
        {
            bool playersSwapped = false;
            if (ContainsPlayer(newPlyr))
            {

            }
            else if (currentPlyr.Position == "QB" && newPlyr.Position == "QB")
            {
                _qb = newPlyr;
                playersSwapped = true;
            }
            else if (currentPlyr.Position == "DST" && newPlyr.Position == "DST")
            {
                _dst = newPlyr;
                playersSwapped = true;
            }
            else if (currentPlyr.Position == "RB" && newPlyr.Position == "RB")
            {
                _rbs.Insert(_rbs.IndexOf(currentPlyr), newPlyr);
                _rbs.Remove(currentPlyr);
                playersSwapped = true;
            }
            else if (currentPlyr.Position == "WR" && newPlyr.Position == "WR")
            {
                _wrs.Insert(_wrs.IndexOf(currentPlyr), newPlyr);
                _wrs.Remove(currentPlyr);
                playersSwapped = true;
            }
            else if (currentPlyr.Position == "TE" && newPlyr.Position == "TE")
            {
                _tes.Insert(_tes.IndexOf(currentPlyr), newPlyr);
                _tes.Remove(currentPlyr);
                playersSwapped = true;
            }
            else if (currentPlyr.Position == "RB" && _rbs.Count == 3)
            {
                if (newPlyr.Position == "WR")
                {
                    _rbs.Remove(currentPlyr);
                    _wrs.Add(newPlyr);
                    playersSwapped = true;
                }
                else if (newPlyr.Position == "TE")
                {
                    _rbs.Remove(currentPlyr);
                    _tes.Add(newPlyr);
                    playersSwapped = true;
                }
            }
            else if (currentPlyr.Position == "WR" && _wrs.Count == 4)
            {
                if (newPlyr.Position == "RB")
                {
                    _wrs.Remove(currentPlyr);
                    _rbs.Add(newPlyr);
                    playersSwapped = true;
                }
                else if (newPlyr.Position == "TE")
                {
                    _wrs.Remove(currentPlyr);
                    _tes.Add(newPlyr);
                    playersSwapped = true;
                }
            }
            else if (currentPlyr.Position == "TE" && _tes.Count == 2)
            {
                if (newPlyr.Position == "RB")
                {
                    _tes.Remove(currentPlyr);
                    _rbs.Add(newPlyr);
                    playersSwapped = true;
                }
                else if (newPlyr.Position == "WR")
                {
                    _tes.Remove(currentPlyr);
                    _wrs.Add(newPlyr);
                    playersSwapped = true;
                }
            }
            else if (_tiers.Count > 0 && currentPlyr.Position == newPlyr.Position)
            {
                _tiers.Insert(_tiers.IndexOf(currentPlyr), newPlyr);
                _tiers.Remove(currentPlyr);
                playersSwapped = true;
            }
            
            else if (currentPlyr == PG)
            {
                if (newPlyr.Position.Contains("PG"))
                {
                    _pgs[0] = newPlyr;
                }
                playersSwapped = true;
            }
            else if (currentPlyr == SG)
            {
                if (newPlyr.Position.Contains("SG"))
                {
                    _sgs[0] = newPlyr;
                }
                playersSwapped = true;
            }
            else if (currentPlyr == SF)
            {
                if (newPlyr.Position.Contains("SF"))
                {
                    _sfs[0] = newPlyr;
                }
                playersSwapped = true;
            }
            else if (currentPlyr == PF)
            {
                if (newPlyr.Position.Contains("PF"))
                {
                    _pfs[0] = newPlyr;
                }
                playersSwapped = true;
            }
            else if (currentPlyr == C)
            {
                if (newPlyr.Position.Contains("C"))
                {
                    _cs[0] = newPlyr;
                }
                playersSwapped = true;
            }
            else if (currentPlyr == G)
            {
                if (newPlyr.Position.Contains("PG"))
                {
                    if (_pgs.Count > 1)
                    {
                        _pgs[1] = newPlyr;
                    }
                    else
                    {
                        _pgs.Insert(1, newPlyr);
                        _sgs.RemoveAt(1);
                    }
                    playersSwapped = true;
                }
                else if(newPlyr.Position.Contains("SG"))
                {
                    if (_sgs.Count > 1)
                    {
                        _sgs[1] = newPlyr;
                    }
                    else
                    {
                        _sgs.Insert(1, newPlyr);
                        _pgs.RemoveAt(1);
                    }
                    playersSwapped = true;
                }
            }
            else if (currentPlyr == F)
            {
                if (newPlyr.Position.Contains("SF"))
                {
                    if (_sfs.Count > 1)
                    {
                        _sfs[1] = newPlyr;
                    }
                    else
                    {
                        _sfs.Insert(1, newPlyr);
                        _pfs.RemoveAt(1);
                    }
                    playersSwapped = true;
                }
                else if (newPlyr.Position.Contains("PF"))
                {
                    if (_pfs.Count > 1)
                    {
                        _pfs[1] = newPlyr;
                    }
                    else
                    {
                        _pfs.Insert(1, newPlyr);
                        _sfs.RemoveAt(1);
                    }
                    playersSwapped = true;
                }
            }
            else if (currentPlyr == UTIL)
            {
                if (_pgs.Contains(currentPlyr))
                {
                    _pgs.Remove(currentPlyr);
                }
                else if (_sgs.Contains(currentPlyr))
                {
                    _sgs.Remove(currentPlyr);
                }
                else if (_sfs.Contains(currentPlyr))
                {
                    _sfs.Remove(currentPlyr);
                }
                else if (_pfs.Contains(currentPlyr))
                {
                    _pfs.Remove(currentPlyr);
                }
                else if (_cs.Contains(currentPlyr))
                {
                    _cs.Remove(currentPlyr);
                }

                if (newPlyr.Position.Contains("PG"))
                {
                    _pgs.Add(newPlyr);
                }
                else if (newPlyr.Position.Contains("SG"))
                {
                    _sgs.Add(newPlyr);
                }
                else if (newPlyr.Position.Contains("SF"))
                {
                    _sfs.Add(newPlyr);
                }
                else if (newPlyr.Position.Contains("PF"))
                {
                    _pfs.Add(newPlyr);
                }
                else if (newPlyr.Position.Contains("C"))
                {
                    _cs.Add(newPlyr);
                }
                playersSwapped = true;
            }
            if (playersSwapped)
            {
                TotalCost -= currentPlyr.Salary;
                currentPlyr.TotalTeams--;
                currentPlyr.MoneySpent -= ContestCost;
                TotalCost += newPlyr.Salary;
                newPlyr.TotalTeams++;
                newPlyr.MoneySpent += ContestCost;
            }

            return playersSwapped;
        }

        public bool TeamContainsAShownPlayer => AllPlayers.Exists(x => x.ShowTeams == true);
        
        public string AllPickemExportString()
        {
            string results = EntryID.ToString()
                + ",," + ContestID.ToString()
                + "," + ContestCost.ToString()
                + "," + T1.StringForCSV()
                + "," + T2.StringForCSV()
                + "," + T3.StringForCSV()
                + "," + T4.StringForCSV()
                + "," + T5.StringForCSV()
                + "," + T6.StringForCSV()
                + "," + T7.StringForCSV()
                + "," + T8.StringForCSV();
            return results;
        }

        public string NFLSalaryExportString()
        {
            string results = EntryID.ToString()
                + ",," + ContestID.ToString()
                + "," + ContestCost.ToString()
                + "," + QB.StringForCSV()
                + "," + RB1.StringForCSV()
                + "," + RB2.StringForCSV()
                + "," + WR1.StringForCSV()
                + "," + WR2.StringForCSV()
                + "," + WR3.StringForCSV()
                + "," + TE.StringForCSV()
                + "," + FLEX.StringForCSV()
                + "," + DST.StringForCSV();
            return results;
        }

        public string NBASalaryExportString()
        {
            string results = EntryID.ToString()
                + ",," + ContestID.ToString()
                + "," + ContestCost.ToString()
                + "," + PG.StringForCSV()
                + "," + SG.StringForCSV()
                + "," + SF.StringForCSV()
                + "," + PF.StringForCSV()
                + "," + C.StringForCSV()
                + "," + G.StringForCSV()
                + "," + F.StringForCSV()
                + "," + UTIL.StringForCSV();
            return results;
        }

    }
}
